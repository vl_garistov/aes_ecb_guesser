/*
 *	AES CTR guesser - Brute force attack against a single block of data encrypted with AES 128 in CTR mode. Intended for demonstration purposes.
 *	Copyright (C) 2024 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::process;

use clap::{Arg, Command};
use clap::error::ErrorKind;
use aes_ctr_guesser::Config;

fn main()
{
	// Declare command-line arguments
	let cli = Command::new("AES ECB guesser")
		.version("0.1.0")
		.author("Vladimir Garistov <vl.garistov@gmail.com>")
		.about("Brute force attack against a single block of data encrypted with AES 128 in ECB mode. Intended for demonstration purposes.")
		.long_about(
			"Brute force attack against a single block of data encrypted with AES 128 in ECB mode. Intended for demonstration purposes.\n\n\
			Requires a block of known plaintext and a corresponding block that is its encrypted counterpart. Works simply by trying every \n\
			possible key to decrypt the encrypted block until the decrypted data matches the known plaintext.\n\
			The encrypted data is passed in as hexadecimal and the plaintext must be a valid UTF-8 string."
		)
		.arg(
			Arg::new("threads")
				.short('n')
				.long("threads")
				.value_name("THREADS")
				.value_parser(clap::value_parser!(u16))
				.default_value("1")
				.help("Number of threads for parallel computation")
				.required(false)
		)
		.arg(
			Arg::new("ciphertext")
				.short('c')
				.long("ciphertext")
				.value_name("CIPHERTEXT")
				.value_parser(clap::value_parser!(String))
				.help("Encrypted data in hexadecimal")
				.required(true)
		)
		.arg(
			Arg::new("plaintext")
				.short('p')
				.long("plaintext")
				.value_name("PLAINTEXT")
				.value_parser(clap::value_parser!(String))
				.help("Known unencrypted data")
				.required(true)
		)
		.arg(
			Arg::new("initial-guess")
				.short('i')
				.long("initial-guess")
				.value_name("KEY")
				.value_parser(clap::value_parser!(String))
				.default_value("00000000000000000000000000000000")
				.help("Initial guess for the key in hexadecimal")
				.required(false))
		.arg(
			Arg::new("initial-vector")
				.short('v')
				.long("initial-vector")
				.value_name("KEY")
				.value_parser(clap::value_parser!(String))
				.default_value("00000000000000000000000000000000")
				.help("Initial vector in hexadecimal")
				.required(false)
		);

	// Parse command-line arguments
	process::exit(match cli.try_get_matches()
	{
		// Construct configuration values from command line arguments
		Ok(arg_matches) => match Config::new(arg_matches)
		{
			// Run the main part of the application
			Ok(config) => match aes_ctr_guesser::run(config)
			{
				Ok(()) => 0,
				Err(err) =>
				{
					eprintln!("Error: {}", err);
					2
				}
			},
			Err(err) =>
			{
				eprintln!("Invalid arguments: {}", err);
				3
			}
		},
		Err(err) =>
		{
			match err.kind()
			{
				ErrorKind::DisplayHelp | ErrorKind::DisplayVersion =>
				{
					println!("{}", err);
					0
				},
				_ =>
				{
					eprintln!("{}", err);
					1
				}
			}
		}
	});
}
