/*
 *	AES CTR guesser - Brute force attack against a single block of data encrypted with AES 128 in CTR mode. Intended for demonstration purposes.
 *	Copyright (C) 2024 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::{Arc, mpsc, Mutex};
use std::sync::mpsc::Sender;

use aes::{Aes128Enc, Block};
use aes::cipher::{BlockEncrypt, KeyInit};
use aes::cipher::generic_array::GenericArray;
use clap::ArgMatches;
use threadpool::ThreadPool;

const BLOCK_SIZE: usize = 16;

#[derive(Debug)]
pub enum GuesserError
{
	WrongCiphertextLength,
	WrongPlainTextLength,
	WrongKeyLength,
	WrongIVLength
}

impl Display for GuesserError
{
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
	{
		match self
		{
			GuesserError::WrongCiphertextLength => write!(f, "invalid length of the ciphertext. It must be exactly 16 bytes (one AES block)."),
			GuesserError::WrongPlainTextLength => write!(f, "invalid length of the plaintext. It must be exactly 16 bytes (one AES block)."),
			GuesserError::WrongKeyLength => write!(f, "invalid length of the initial guess for the key. It must be exactly 16 bytes (one AES block)."),
			GuesserError::WrongIVLength => write!(f, "invalid length of the initial vector. It must be exactly 16 bytes (one AES block).")
		}
	}
}

impl Error for GuesserError{}

pub struct Config
{
	pub threads: u16,
	pub ciphertext: u128,
	pub plaintext: u128,
	pub initial_key_guess: u128,
	pub initial_vector: u128
}

impl Config
{
	#[allow(non_snake_case)]
	pub fn new(arguments: ArgMatches) -> Result<Config, Box<dyn Error>>
	{
		// The following unwrap()s never panic because either a default value is provided in main.rs or the argument is mandatory.
		let threads = *arguments.try_get_one::<u16>("threads")?.unwrap();
		
		let ciphertext_string = arguments.try_get_one::<String>("ciphertext")?.unwrap().clone();
		let ciphertext_string = ciphertext_string.replace("0x", "");
		let ciphertext_string = ciphertext_string.replace("_", "");
		let ciphertext_vec = hex::decode(ciphertext_string)?;
		let ciphertext: [u8; BLOCK_SIZE] = match ciphertext_vec.try_into()
		{
			Ok(vec_data) => vec_data,
			Err(_) => return Err(Box::new(GuesserError::WrongCiphertextLength))
		};
		let ciphertext = u128::from_be_bytes(ciphertext);
		
		let plaintext_string = arguments.try_get_one::<String>("plaintext")?.unwrap();
		let plaintext: [u8; BLOCK_SIZE] = match plaintext_string.as_bytes().try_into()
		{
			Ok(vec_data) => vec_data,
			Err(_) => return Err(Box::new(GuesserError::WrongPlainTextLength))
		};
		let plaintext = u128::from_be_bytes(plaintext);
		
		let init_string = arguments.try_get_one::<String>("initial-guess")?.unwrap().clone();
		let init_string = init_string.replace("0x", "");
		let init_string = init_string.replace("_", "");
		let init_vec = hex::decode(init_string)?;
		let init: [u8; BLOCK_SIZE] = match init_vec.try_into()
		{
			Ok(vec_data) => vec_data,
			Err(_) => return Err(Box::new(GuesserError::WrongKeyLength))
		};
		let initial_key_guess = u128::from_be_bytes(init);
		
		let IV_string = arguments.try_get_one::<String>("initial-vector")?.unwrap().clone();
		let IV_string = IV_string.replace("0x", "");
		let IV_string = IV_string.replace("_", "");
		let IV_vec = hex::decode(IV_string)?;
		let IV: [u8; BLOCK_SIZE] = match IV_vec.try_into()
		{
			Ok(vec_data) => vec_data,
			Err(_) => return Err(Box::new(GuesserError::WrongIVLength))
		};
		let initial_vector = u128::from_be_bytes(IV);
		
		Ok(Config {
			threads,
			ciphertext,
			plaintext,
			initial_key_guess,
			initial_vector
		})
	}
}

struct WorkerConf
{
	pub num_threads: u16,
	pub result_tx: Sender<u128>,
	pub done_flag: Arc<Mutex<bool>>,
	pub ciphertext: u128,
	pub plaintext: u128,
	pub initial_key_guess: u128,
	pub initial_vector: u128
}

// Main part of the program
pub fn run(config: Config) -> Result<(), Box<dyn Error>>
{
	let pool = ThreadPool::new(config.threads as usize);
	let (tx, rx) = mpsc::channel();
	let key_found = Arc::new(Mutex::new(false));
	
	for i in 0..config.threads
	{
		let conf = WorkerConf {
			num_threads: config.threads,
			result_tx: tx.clone(),
			done_flag: Arc::clone(&key_found),
			ciphertext: config.ciphertext,
			plaintext: config.plaintext,
			initial_key_guess: config.initial_key_guess + i as u128,
			initial_vector: config.initial_vector
		};
		pool.execute(move || {guess_key(conf)});
	}
	
	let key = rx.recv()?;
	*key_found.lock().unwrap() = true;
	pool.join();
	println!("Key: {:X}", key);

	Ok(())
}

fn guess_key(conf: WorkerConf)
{
	let in_block = Block::from(conf.initial_vector.to_be_bytes());
	let mut out_block = Block::from([0; BLOCK_SIZE]);
	let mut key: u128 = conf.initial_key_guess;

	loop
	{
		for _ in 0..1000
		{
			let cipher = Aes128Enc::new(GenericArray::from_slice(&key.to_be_bytes()));
			
			cipher.encrypt_block_b2b(&in_block, &mut out_block);
			if u128::from_be_bytes(out_block.into()) ^ conf.ciphertext == conf.plaintext
			{
				conf.result_tx.send(key).unwrap();
				break;
			}
			key = key + conf.num_threads as u128;
		}
		if *conf.done_flag.lock().unwrap()
		{
			break;
		}
	}
}
