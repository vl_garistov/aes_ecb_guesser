# AES CTR guesser

Brute force attack against a single block of data encrypted with AES 128 in CTR mode. Intended for demonstration purposes.

Requires a block of known plaintext and a corresponding block that is its encrypted counterpart. Works simply by trying every
possible key to decrypt the encrypted block until the decrypted data matches the known plaintext.
The encrypted data, initial vector and initial key guess are passed in as hexadecimal and the plaintext must be a valid UTF-8 string.

## Usage
```
Usage: aes_ctr_guesser [OPTIONS] --ciphertext <CIPHERTEXT> --plaintext <PLAINTEXT>

Options:
  -n, --threads <THREADS>        Number of threads for parallel computation [default: 1]
  -c, --ciphertext <CIPHERTEXT>  Encrypted data in hexadecimal
  -p, --plaintext <PLAINTEXT>    Known unencrypted data
  -i, --initial-guess <KEY>      Initial guess for the key in hexadecimal [default: 00000000000000000000000000000000]
  -v, --initial-vector <KEY>     Initial vector in hexadecimal [default: 00000000000000000000000000000000]
  -h, --help                     Print help (see more with '--help')
  -V, --version                  Print version
```

## Example

Ciphertext: 0xDBB4_2CB1_44E7_7A80_C752_8B81_2243_DFB7

Plaintext: poBaiT060v0vreme

Initial key guess: 0xACAB_ACAB_ACAB_ACAB_ACAB_ACAB_AC00_0000

Initial vector: 0x0000_0000_0000_0000_0000_0000_0000_0000
